# AIRLINE SERVICE
School project at CTU in Prague. The implementation meets the following requirements.

## Assigment
Design and develop a web service for aircraft company which allows the clients to plan and order flights. In the project you will design the RESTful Airline Service, you will connect this service to other existing services. Part of the application is also a thick client implemented as a web page allowing the user to access the Airline Service. The client *must* be able to demonstrate all the functionality.

## REST - Airline service
You will design a service providing the following REST resources

* Flight
* Destination
* Reservation

The first three resources will provide the CRUD (Create Read Update Delete) opearations. The last source will support only the Create operation (using the POST method).

### Destination Resource

The resource is defined as follows:
       
- URI: /destination (GET, POST), /destination/{id} (GET, PUT, DELETE)
- Methods:
    - List of all destinations - GET (URI: /destination). Detail: GET( URI: /destination/{id})
    - Create destination - POST (URI: /destination)
    - Modify destination - PUT (URI: /destination/{id})
    - Delete destination - DELETE (URI: /destination/{id})
- Destination attributes:
    - id - identificator (Long)
    - name - destination name (unikátní)
    - url - destination url (př. /destination/1)
    - lat - latitude (float)
    - lon - longitude (float)
- format: both XML and JSON
- All methods will return JSON/XML with attributes defined above and source URL

Methods POST, PUT and DELETE will be secured. The methods may be called only by users with the “admin” user role.

### Flight Resource

Flight resource provides a list of flights and allows the user to modify the flights.

- URI: /flight (GET, POST), /flight/{id} (GET, PUT, DELETE)
- Methods:
    - List of all flights - GET (URI list of flights: /flight, URI one flight: /flight/{id})
    - Create a flight - POST (URI: /flight)
    - Change flight info - PUT (URI: /flight/{id})
    - Delete a flight - DELETE (URI: /flight/{id})
- Attributes:
    - id - identificator (Long)
    - name - flight name (unikátní)
    - dateOfdeparture - date and time of departure (format iso8601 YYYY-MM-DDThh:mm:ssTZD)
    - distance - distance in km
    - seats - number of seats in the aircraft
    - price - proce of the flight
    - from - from destination - id of the flight is sent when the destination is created only
    - to - final destination - id of the flight is sent when the destination is created only
    - url - url of the resource (př. /flight/1)
- format: XML, JSON
- All methods will return JSON/XML with attributes defined above and source URL

The security of this resource is the same like for the destination. The POST, PUT and DELETE operations are secured. Caution! It is necessary to connect the resurces between the flights and destination resources (using URL address of destinations).
#### Filtering, pagination, sorting
Use the “X-Filter” header to filter the flights in the FROM, TO manner.
```
X-Filter: dateOfDepartureFrom=2013-02-27T02:04:46+01:00,dateOfDepartureTo=2013-02-27T03:04:46+01:00
```

It will be also possible to order the values.  
```
X-Order: dateOfDeparture:asc    nebo     X-Order: name:desc
```

It does not make sence to send all data at once, therefore pagination is used. Pagination will be created using the X-Base and X-Offset headers. Base is the number of requested records, offset is the beginning of the records. If the client requests 10 records starting from the 20tieth record, he adds the following headers to the request:

```
X-Base: 10 X-Offset: 20
```

If the request doesn't contain these headers, server sends all data. For the client to know the number of records he can get the server sends the X-Count-records header together with the data: X-Count-records: 18

### Reservation
The clients create reservations for flights. New reservation starts with the state new. Once the reservation is paid (the client presses the pay button), the reservation goes to the paid state. The user can cancel the reservation using the PUT method - the state is changed to cancel. Only reservation in the state “new” can be canceled.

When the reservation is created, random password is sent to the client. When the client wishes to modify the reservation, he needs to input the password.

It is necessary to consider number of seats in the aircraft and allow the users to reserve their places accordingly. Don't forget to connect the flights with destinations.

- URI: /reservation (GET, POST), /reservation/{reservationId} (GET, PUT, DELETE)
- Methods:
    - List - GET (URI list of reservations: /reservation, URI one reservation: /reservation/{id})
    - Create - POST (URI: /reservation)
    - Update - PUT (URI: /reservation/{id})
    - Delete - DELETE (URI: /reservation/{id})
- Attributes:
    - id - identificator (Long)
    - flight - flight id
    - created - date and time of reservation creation
    - password - randomly generated password
    - seats - number of the seats of the reservation
    - state - reservation state (new, canceled, paid)
    - url - url address of the resource
- format: XML, JSON
- All methods will return JSON/XML with attributes defined above and source URL

Securing the source is a bit more complicated. The list of the reservations (GET method) may see only the user with the role “admin” or “manager”. The same applies to the DELETE operation for a given resource. To get one reservation /reservation/{id} the user has to know the password (send the “X-Password” header). The same applies to the PUT method.

### Client

The client will be realised using HTML, JavaScript or any other suitable technology. It does not have to be nice or complicated. However, the client has to implement all the functionality provided by the server - sorting, filtering, pagination.

## Third parties
### Google API
Before the new destination is created, get the coordinates of the destination using the https://developers.google.com/maps/documentation/geocoding/. Store the coordinates to the destination.

### Flight distance API
When creating the new flight use the service which computes the distance between two points at the map Rome2Rio. The service may find other modes of transport than air travel. We are interested in the length of the final route. Use the distance to compute the price of the ticket. 100 km = 1000 CZK.

## WebSockets: Number of connected clients
Implement visualisation of number of currently connected clients in the client part using Websockets.

## JAX-WS Printing Service
Printing Service is a service for automatic generation of e-tickets. It accepts the object of the reservation as a parameter. The reservation has to be paid.

- Use the JAX-WS bottom-up method to design a service which returns simple text file as a response to the request for printing.
- Use the JAX-WS top-down method to design a service which sends the tickets to the email. The service gets an email address as an input and does not return anything. When an email is received, the service generates a JMS message and sends it to the channel. The message contains a generated file and a mail to which the file should be sent.
- On the same channel there is a JMS consumer listener which processes the received messages. Processing means printing the output to the console.

## DEPLOY script
```
#!/bin/bash
mvn package
<path>/glassfish4/bin/asadmin redeploy --name <appName> target/*.war
```