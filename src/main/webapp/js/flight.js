



    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var URL = "service/flight/";
    var BASE, OFFSET, FROM, TO, ORDERING;
    var FLIGHT_ATTRIBUTES_ALL = ["id", "name", "from", "to", "dateOfDeparture", "price", "seats", "distance"];
    var FLIGHT_ATTRIBUTES_SEND = ["name", "from", "to", "dateOfDeparture", "seats"];
    var DATE_TIME_PICKER_SETTINGS = {timeFormat: 'HH:mm:ss', dateFormat: 'yy-mm-ddT', showTimezone: false};

    function downloadAndShowFlightDetail(id) {
        $.ajax({
            url: URL + id,
            type: "GET",
            Accept: "application/json",
            cache: false,
            dataType: "json",
            headers: {
            },
            success: function (response) {
                showFlightDetail(response);
            },
            complete: function (xhr, textStatus) {
                $("#responseCode").html(xhr.status);
            }
        });
    }
    function deleteFlight(id){
        $.ajax({
            url: URL + id,
            type: "DELETE",
            Accept: "application/json",
            cache: false,
            dataType: "json",
            headers: {
            },
            complete: function (xhr, textStatus) {
                downloadAndShowFlightList();
                $("#responseCode").html(xhr.status);
            }
        });
    }
    /**
    *
    * @param flight set to undefined if creating a new flight.
     */
    function showFlightDetail(flight) {
        $("#content").empty();
        var $form = $("<form>").addClass("pure-form pure-form-stacked");
        $("#content").append($form);
        $.each(FLIGHT_ATTRIBUTES_ALL, function (key, attrName) {
            var $label = $("<label>").text(attrName);
            var $input;
            if (flight == undefined) {//creating new flight
                $input = $("<input>");
            } else {
                $input = $("<input>").val(flight[attrName]);
            }
            if (attrName == "id") {
                $input.prop('disabled', true);
            } else if (attrName == "dateOfDeparture") {
                if(flight !=undefined){
                $input.val(Util.prepareTimeForClient(flight[attrName]));}
                $input.datetimepicker(DATE_TIME_PICKER_SETTINGS);
            }
            $input.attr("name", attrName);
            $label.attr("for", attrName);
            $form.append($label).append($input);
        });
        var $button = $("<button>").attr("type", "button");
        $button.text("submit").addClass("pure-button pure-button-primary");
        $button.click(function () {
            var dataToSubmit = {};
            $.each(FLIGHT_ATTRIBUTES_SEND, function (key, attrName) {
                var newValue = $form.find("input[name=" + attrName + "]").val();
                if (attrName == "dateOfDeparture") {
                    newValue = Util.prepareTimeForServer(newValue);
                }
                dataToSubmit[attrName] = newValue;
            });
            var flightId = flight == undefined ? undefined : flight.id;
            sendFlight(dataToSubmit,flightId);

        });
        $form.append($button);
    }
    var Util = {
        /**
         * prepares string from server for client (namely, datetimepicker jquery addon). It must have space after T and no time zone.
         * @param timeString
         * @returns {string}
         */
        prepareTimeForClient: function (timeString) {
            var preparedString = timeString.replace("T", "T ");
            return preparedString.substring(0, preparedString.indexOf("+"));
        },
        prepareTimeForServer: function (timeString) {
            var preparedString = timeString.replace("T ", "T");
            return preparedString;
        }

    }
    function sendFlight(data, id) {
        var fullUrl = URL;
        if (id != undefined) {
            fullUrl += id;
        }
        var method = id == undefined ? "POST" : "PUT";
        $.ajax({
            url: fullUrl,
            type: method,
            data: JSON.stringify(data),
            contentType: "application/json",
            cache: false,
            dataType: "json",
            headers: {
                'username': $("#username").val(),
                'password': $("#password").val(),
            },
            complete: function (xhr, textStatus) {
                $("#responseCode").html(xhr.status);
            }
        });
        return false;
    }

    function showFlightList(flights) {
        $("#content").empty();
        var $paginationDiv = $("<div>");
        $paginationDiv.append($("<span>").text("base:"));
        $paginationDiv.append($("<input>").attr("id","base").val(BASE));
        $paginationDiv.append($("<span>").text(" offset:"));
        $paginationDiv.append($("<input>").attr("id","offset").val(OFFSET));
        var $filterDiv = $("<div>");
        $filterDiv.append($("<span>").text("from:"));
        $filterDiv.append($("<input>").attr("id", "from").val(FROM).datetimepicker(DATE_TIME_PICKER_SETTINGS));
        $filterDiv.append($("<span>").text(" to:"));
        $filterDiv.append($("<input>").attr("id", "to").val(TO).datetimepicker(DATE_TIME_PICKER_SETTINGS));
        $filterDiv.append($("<br>"));
        $filterDiv.append($("<span>").text("ordering:"));
        $filterDiv.append($("<input>").attr("id", "ordering").val(ORDERING));
        $filterDiv.append($("<span>").text("(dateOfDeparture:asc nebo name:desc)"));
        var $filterButton = $("<button>").attr("type", "button").text("Filter!").addClass("pure-button pure-button-primary");
        $filterButton.click(function () {
            OFFSET = $paginationDiv.find("#offset").val()
            BASE = $paginationDiv.find("#base").val()
            FROM = $filterDiv.find("#from").val();
            TO = $filterDiv.find("#to").val();
            ORDERING = $filterDiv.find("#ordering").val();
            console.log(FROM);
            var fromPrepared, toPrepared;
            var filterHeaders = {};
            var fromToFilter = [];
            if (FROM != "") {
                fromPrepared = Util.prepareTimeForServer(FROM);
                fromToFilter.push("dateOfDepartureFrom=" + fromPrepared);
            }
            if (TO != "") {
                toPrepared = Util.prepareTimeForServer(TO);
                fromToFilter.push("dateOfDepartureTo=" + toPrepared);
            }
            if (fromToFilter.length > 0) {
                filterHeaders["X-Filter"] = fromToFilter.join(",");
            }
            if (ORDERING != "") {
                filterHeaders["X-Order"] = ORDERING;
            }
            filterHeaders["X-Base"] = BASE;
            filterHeaders["X-Offset"] = OFFSET;
            downloadAndShowFlightList(filterHeaders);
        });
        $("#content").append($paginationDiv);
        $("#content").append($filterDiv);
        $("#content").append($filterButton);
        var $table = $("<table>");
        $table.addClass("pure-table");
        if (flights.length > 0) {//generate table header
            var $thead = $("<thead>");
            var $tr = $("<tr>");
            $.each(FLIGHT_ATTRIBUTES_ALL, function (key, attribute) {
                $tr.append($("<th>").text(attribute));
            });
            $tr.append($("<th>").text("actions"));
            $thead.append($tr);
            $table.append($thead);
        }
        $.each(flights, function (i, flight) {//generate rows
            var $tr = $('<tr>');
            $.each(FLIGHT_ATTRIBUTES_ALL, function (key, attribute) {
                $tr.append($('<td>').text(flight[attribute]));
                $table.append($tr);
            });
            var $actions = $("<td>");
            var $detailButton = $("<a>").addClass("pure-button pure-button-active").text("edit");
            $detailButton.click(function () {
                downloadAndShowFlightDetail(flight.id);
            });
            $actions.append($detailButton);
            var $deleteButton = $("<a>").addClass("pure-button pure-button-active").text("delete");
            $deleteButton.click(function () {
                deleteFlight(flight.id);
            });
            $actions.append($deleteButton);
            $tr.append($actions);
        });
        var $button = $("<button>").attr("type", "button");
        $button.text("add new").addClass("pure-button pure-button-primary");
        $button.click(function(){
            showFlightDetail(undefined);
        })
        $("#content").append($table).append($button);
    }

    function downloadAndShowFlightList(filterHeaders) {
        var headers =  {
        };
        if(filterHeaders != undefined){
            $.each(filterHeaders,function(key,value){
               headers[key] = value;
            });
        }
        console.log(headers);
        $.ajax({
            url: URL,
            type: "GET",
            Accept: "application/json",
            cache: false,
            dataType: "json",
            headers: headers,
            success: function (response) {
                showFlightList(response);
            },
            complete: function (xhr, textStatus) {
                $("#responseCode").html(xhr.status);
            }
        });
    }