/**
 * Created by Jan on 10.11.2015.
 */
function showGetReservationForm(){
    /**
     * Input form to get detail of existing reservation via generated password
     */

    var form='<form class="pure-form pure-form-stacked" onsubmit="checkPass(this);return false;">' +
        '<fieldset>'+
        '<legend>Insert your reservation id and password</legend>'+
        '<label for="id">Reservation id</label>'+
        '<input id="id" type="text" placeholder="Reservation id">'+
        '<label for="pass">Reservation code</label>'+
        '<input id="pass" type="text" placeholder="Reservation password">'+
        '<button type="submit" class="pure-button pure-button-primary">Open reservation</button>'+
        '</fieldset></form>';

    /**
     * Input form to create new reservation
     */

    form+='<form class="pure-form pure-form-stacked" id="newReservationForm" onsubmit="newReservation(this);return false;">' +
        '<fieldset>'+
        '<legend>Create new reservation</legend>'+
        '<label for="flight">ID of the flight</label>'+
        '<input id="flight" type="text" placeholder="ID of the flight">'+
        '<label for="seats">Number of seats</label>'+
        '<input id="seats" type="number" value="1" >'+
        '<button type="submit" class="pure-button pure-button-primary">Create reservation</button>'+
        '</fieldset></form>';
    /**
     * Buttons to access admin functions
     */
    form+='</br></br><a class="pure-button pure-button-active" href="javascript:downloadAndShowReservations()">Show list fo reservations</a>';

    $("#content").html(form);
}

function downloadAndShowReservations(){
    $.ajax({
        url: "service/reservation/",
        type: "GET",
        Accept: "application/json",
        cache: false,
        dataType: "json",
        headers: {
        },
        success : function(response) {
            showReservationList(response)
        }
    });
}



function showNewReservationForm(){

    var form='<form class="pure-form pure-form-stacked" id="newReservationForm" onsubmit="newReservation(this);return false;">' +
        '<fieldset>'+
        '<legend>Create new reservation</legend>'+
        '<label for="flight">ID of the flight</label>'+
        '<input id="flight" type="text" placeholder="ID of the flight">'+
        '<label for="seats">Number of seats</label>'+
        '<input id="seats" type="number" value="1" >'+
        '<button type="submit" class="pure-button pure-button-primary">Create reservation</button>'+
        '</fieldset></form>';
    $("#content").html(form);

}

function newReservation(data){
    if(validateFormAllRequired("#newReservationForm")==false) return false;
    $.ajax({
        url: "service/reservation/",
        type: "POST",
        Accept: "application/json",
        data: JSON.stringify({
            flight: data.flight.value,
            seats: data.seats.value
        }),
        contentType: "application/json",
        cache: false,
        dataType: "json",
        headers: {
        },
        complete: function(xhr, textStatus) {
            $("#responseCode").html(xhr.status);
            var response=JSON.parse(xhr.responseText);
            if(xhr.status==200)alert("Reservation"+response.id+" added. Your passoword is: "+response.password);
            if(xhr.status==403)alert("Flight is full or not existing");
        }
    });
    return false;
}


function showReservationList(reservations) {
    var out = '<table class="pure-table"> <thead> <tr> <th>#</th> <th>Flight</th> <th>Created</th> <th>Seats</th> <th>State</th><th>Action</th></tr> </thead>';
    var i;
    for(i = 0; i < reservations.length; i++) {
        out += '<tr><td><a class="pure-button pure-button-active" href="javascript:downloadAndShowReservationDetail('+reservations[i].id+')">'
            +reservations[i].id+'</a></td><td>'+ reservations[i].flight +'</td><td>' + reservations[i].created + '</td><td>' + reservations[i].seats +'</td><td>' + reservations[i].state +'</td><td>'
            +'<a class="pure-button pure-button-active" href="javascript: deleteReservation('+reservations[i].id+')">Delete</a>'+
            '</tr>';
    }
    out+='</table></br><a class="pure-button pure-button-active" href="javascript:showNewReservationForm()">new reservation</a>';
    $("#content").html(out);

}
function showReservationDetail(reservation) {
    var out = '<table class="pure-table"> <thead> <tr> <th>#</th> <th>Flight</th> <th>Created</th> <th>Seats</th> <th>State</th><th>print</th></tr> </thead>';
    var i;

    out += '<tr><td>' + reservation.id +'</td><td>' + reservation.flight +'</td><td>' + reservation.created + '</td><td>' + reservation.seats +'</td> <td>' + reservation.state +'</td><td><a href="#" class="download" onclick="downloadETicket('+reservation.id+')">download</a></td></tr>';

    out+="</table>";

    out+='</br></br><a class="pure-button pure-button-active" href="javascript: payReservation('+reservation.id+')">Pay reservation</a><a class="pure-button pure-button-active" href="javascript: cancelReservation('+reservation.id+')">Cancel reservation</a></br>';
    out+='<label for="pass">Reservation code</label>'+
        '<form class="pure-form"><input id="pass" type="text" placeholder="Reservation password"></br>';
    out+='<label for="email">Email</label>'+
        '<input id="email" type="text" placeholder="Email"><a class="pure-button pure-button-active" href="javascript: sendReservation('+reservation.id+')">Send reservation ticket</a></form>';
    $("#content").html(out);

}

function checkPass(data){
    $.ajax({
        url: "service/reservation/"+data.id.value,
        type: "GET",
        Accept: "application/json",
        cache: false,
        dataType: "json",
        headers: {
            'X-password':data.pass.value,
        },
        complete: function(xhr, textStatus) {
            $("#responseCode").html(xhr.status);
            if(xhr.status!=200){
                alert("Reservation in not existing or password is wrong.");
                return;
            }
            showReservationDetail(JSON.parse(xhr.responseText));
        }
    });

}

function deleteReservation(id){
    $.ajax({
        url: "service/reservation/"+id,
        type: "DELETE",
        cache: false,
        dataType: "json",
        headers: {
        },
        complete: function(xhr, textStatus) {
            $("#responseCode").html(xhr.status);
            downloadAndShowReservations();
        }
    });
}

function downloadAndShowReservationDetail(id){
    if (undefined == id)  return;

    url= "service/reservation/"+id;
    xmlhttp.onreadystatechange = function(){
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            var reservation=JSON.parse(xmlhttp.responseText);
            showReservationDetail(reservation);
        }
    }
    xmlhttp.open('GET', url,true);
    xmlhttp.setRequestHeader('Accept','application/json');
    xmlhttp.send();
}
/**
 * using jquery wont prompt user to save the file. it can be demonstrated when logged in as ADMIN/MANAGER and copied the
 * url directly to address bar. No need to set the header X-password then.
 * @param id
 */
function downloadETicket(id){
    window.location="service/reservation/"+id+"/print?X-password="+$("#pass").val();

   /* $.ajax({
        url: "service/reservation/"+id+"/print",
        type: "GET",
        cache: false,
        headers: {'X-password':($("#pass").val())
        },

        complete: function(xhr, textStatus) {
            $("#responseCode").html(xhr.status);
            checkPass({"id":{"value":id},"pass":{"value":$("#pass").val()}});
        }
    });*/
}


function sendReservation(id){
    $.ajax({
        url: "service/reservation/"+id+"/send",
        type: "POST",
        cache: false,
        dataType: "application/json",
        contentType: "application/json",
        params: {'X-password':($("#pass").val())
        },
        data: {
            'email':($("#email").val())
        },
        complete: function(xhr, textStatus) {
            $("#responseCode").html(xhr.status);
            checkPass({"id":{"value":id},"pass":{"value":$("#pass").val()}});
        }
    });
}

function payReservation(id){
    $.ajax({
        url: "service/reservation/"+id+"/payment",
        type: "POST",
        cache: false,
        dataType: "application/json",
        contentType: "application/json",
        data: JSON.stringify({
            cardNumber: 122
        }),
        params: {'X-password':($("#pass").val())
        },
        complete: function(xhr, textStatus) {
            $("#responseCode").html(xhr.status);
            checkPass({"id":{"value":id},"pass":{"value":$("#pass").val()}});
        }
    });
}

function cancelReservation(id){
    $.ajax({
        url: "service/reservation/"+id,
        type: "PUT",
        cache: false,
        dataType: "json",
        headers: {
            'X-password':($("#pass").val())
        },
        complete: function(xhr, textStatus) {
            $("#responseCode").html(xhr.status);
            checkPass({"id":{"value":id},"pass":{"value":$("#pass").val()}});
        }
    });
};