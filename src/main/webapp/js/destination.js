
            var xmlhttp = new XMLHttpRequest();
            var url = "service/destination";





            function downloadAndShowDestinationDetail(id){
                $.ajax({
                    url: "service/destination/"+id,
                    type: "GET",
                    Accept: "application/json",
                    cache: false,
                    dataType: "json",
                    headers: {
                    },
                    success : function(response) {
                        showEditDestinationForm(response);
                    }
                });
            }

            function downloadAndShowDestinations(filterHeaders){

                var headers =  {
                };
                if(filterHeaders != undefined){
                    $.each(filterHeaders,function(key,value){
                        headers[key] = value;
                    });
                }

                $.ajax({
                    url: "service/destination/",
                    type: "GET",
                    Accept: "application/json",

                    cache: false,
                    dataType: "json",
                    headers: headers,
                    success : function(response) {
                        showDestinationList(response)
                    }
                });
            }

            function showEditDestinationForm(dest){
                var form='<form class="pure-form pure-form-stacked" id="updateDestinationForm"onsubmit="updateDest(this);return false;">' +
                        '<fieldset>'+
                        '<legend>Add new destination</legend>'+
                        '<label for="id">ID</label>'+
                        '<input id="id" type="text" placeholder="Id of destination" disabled value="'+dest.id+'">'+
                        '<label for="name">Name</label>'+
                        '<input id="name" type="text" placeholder="Name of destination" value="'+dest.name+'">'+
                        '<label for="lat">Latitude</label>'+
                        '<input id="lat" type="number" placeholder="Latitude" value="'+dest.lat+'">'+
                        '<label for="lon">Longtitude</label>'+
                        '<input id="lon" type="number" placeholder="Longtitude"value="'+dest.lon+'" >'+
                        '<button type="submit" class="pure-button pure-button-primary">Update destination</button>'+
                        '</fieldset></form>';

                $("#content").html(form);

            }

            function updateDest(data){
                if(validateFormAllRequired("#updateDestinationForm")==false) return false;
                $.ajax({
                    url: "service/destination/"+data.id.value,
                    type: "PUT",
                    data: JSON.stringify({
                        name: data.name.value,
                        lon: data.lon.value,
                        lat: data.lat.value
                    }),
                    contentType: "application/json",
                    cache: false,
                    dataType: "json",
                    headers: {
                    },
                    complete: function(xhr, textStatus) {
                        $("#responseCode").html(xhr.status);
                    }
                });
                return false;
            }

            function showNewDestinationForm(){




                var form='<form class="pure-form pure-form-stacked" id="newDestinationForm" onsubmit="newDest(this); return false;">' +
                        '<fieldset>'+
                        '<legend>Add new destination</legend>'+
                        '<label for="name">Name</label>'+
                        '<input id="name" type="text" placeholder="Name of destination">'+
                        '<button type="submit" class="pure-button pure-button-primary">Add new</button>'+
                        '</fieldset></form>';

                $("#content").html(form);



            }

            function newDest(data){
                if(validateFormAllRequired("#newDestinationForm")==false) return false;
                $.ajax({
                    url: "service/destination/",
                    type: "POST",
                    data: JSON.stringify({
                        name: data.name.value,
                    }),
                    contentType: "application/json",
                    cache: false,
                    dataType: "json",
                    headers: {
                    },
                    complete: function(xhr, textStatus) {
                        $("#responseCode").html(xhr.status);
                        if(xhr.status==200)alert("Destination added.");
                        else if(xhr.status==403)alert("Error");
                    }

                });
                return false;
            }


            function showDestinationList(dests) {
                $("#content").empty();
                var $ascButton = $("<button>").attr("type", "button").text("ASC").attr("class","pure-button pure-button-primary");
                var $descButton = $("<button>").attr("type", "button").text("DESC").attr("class","pure-button pure-button-primary");
                $ascButton.click(function () {
                    filterHeaders = {"X-Order":"asc"};
                    downloadAndShowDestinations(filterHeaders);
                });
                $descButton.click(function () {
                    filterHeaders = {"X-Order":"desc"};
                    downloadAndShowDestinations(filterHeaders);
                });
                $("#content").append($ascButton);
                $("#content").append($descButton);
                var out = '<table class="pure-table"> <thead> <tr> <th>#</th> <th>Name</th> <th>Latitude</th> <th>Longtitude</th><th>Action</th> </tr> </thead>';
                var i;
                for(i = 0; i < dests.length; i++) {
                    out += '<tr><td><a class="pure-button pure-button-active" href="javascript: downloadAndShowDestinationDetail('+dests[i].id+')">'
                            +dests[i].id+'</a></td><td>'+ dests[i].name +'</td><td>' + dests[i].lat + '</td><td>' + dests[i].lon +'</td><td>'
                            +'<a class="pure-button pure-button-active" href="javascript: deleteDest('+dests[i].id+')">Delete</a>'+
                            '<a class="pure-button pure-button-active" href="javascript: downloadAndShowDestinationDetail('+dests[i].id+')">Update</a></td></tr>';
                }
                out+='</table></br><a class="pure-button pure-button-active" href="javascript: showNewDestinationForm()">new destination</a>';
                $("#content").append(out);
            }
            function showDestinationDetail(dest) {
                var out = '<table class="pure-table"> <thead> <tr> <th>#</th> <th>Name</th> <th>Latitude</th> <th>Longtitude</th></tr> </thead>';
                var i;

                out += '<tr><td>' + dest.id +'</td><td>' + dest.name +'</td><td>' + dest.lat + '</td><td>' + dest.lon +'</td></tr>';

                out+="</table>";
                $("#content").html(out);

            }
            function deleteDest(id){
                $.ajax({
                    url: "service/destination/"+id,
                    type: "DELETE",
                    cache: false,
                    dataType: "json",
                    headers: {
                    },
                    complete: function(xhr, textStatus) {
                        $("#responseCode").html(xhr.status);
                        downloadAndShowDestinations();
                    }
                });
            }
