<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
    <title>AOSFlights - flight reservation service</title>


    <script src="<%=request.getContextPath() %>/js/jquery-1.11.2.min.js"></script>
    <script src="<%=request.getContextPath() %>/js/commonscripts.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet"  href="https://cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.4.5/jquery-ui-timepicker-addon.min.css">
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.4.5/jquery-ui-timepicker-addon.min.js"></script>

    <script src="<%=request.getContextPath() %>/js/reservation.js"></script>
    <script src="<%=request.getContextPath() %>/js/destination.js"></script>
    <script src="<%=request.getContextPath() %>/js/flight.js"></script>

</head>

<body style="width: 900px; margin: 5px auto; border:1px solid #CBCBCB; border-radius: 5px; padding: 10px;">

<div class="pure-menu pure-menu-horizontal">
    <a href="javascript:showHome()" class="pure-menu-heading pure-menu-link">Home</a>
    <ul class="pure-menu-list">
        <li class="pure-menu-item"><a href="javascript:showDestination()" class="pure-menu-link">Destinations</a></li>
        <li class="pure-menu-item"><a href="javascript:showFlight()" class="pure-menu-link">Flight</a></li>
        <li class="pure-menu-item"><a href="javascript:showReservation()" class="pure-menu-link">Reservations</a></li>
    </ul>
    online: <span id="onlineCount">?</span>
</div>


<h1>AOSFlights - flight reservation service</h1>
<!--Username: <input id="username" type="text" value="admin">, password: <input id="password" type="text" value="admin"> <br>-->
Response code: <span id="responseCode"></span>
<h2 id="header">Welcome!</h2>
<div id=content></div>

<script>
    var xmlhttp = new XMLHttpRequest();
    var url = "/";

    function websocket() {
        var websocket = new WebSocket("ws://localhost:8080/websocket/echo");
        websocket.onmessage = function (evt) {
            $("#onlineCount").html(evt.data);
            console.log(evt);
        };
        websocket.onerror = function (evt) {
            $("#onlineCount").html("error");
        };
    }
    websocket();


    function showHome(){
        $("#header").html("Welcome!");
        $("#content").html("");
    }

    function showReservation(){

        $("#header").html("Reservations section");
        showGetReservationForm();
    }

    function showDestination(){

        $("#header").html("Destinations section");
        filterHeaders = {"X-Order":"desc"};
        downloadAndShowDestinations(filterHeaders);
    }

    function showFlight(){
        $("#header").html("Flights section");
        downloadAndShowFlightList();
    }

</script>

</body>

</html>