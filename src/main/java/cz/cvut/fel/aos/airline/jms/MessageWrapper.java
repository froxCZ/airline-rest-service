package cz.cvut.fel.aos.airline.jms;

import java.io.Serializable;

/**
 * Created by Jan on 08.01.2016.
 */
public class MessageWrapper implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    public MessageWrapper(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ObjectMessage{name='" + name + "'}";
    }
}
