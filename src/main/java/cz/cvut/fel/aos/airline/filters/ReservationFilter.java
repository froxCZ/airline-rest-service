package cz.cvut.fel.aos.airline.filters;

/**
 * Created by Jan on 15.12.2015.
 */

import com.sun.jersey.api.uri.UriTemplate;
import cz.cvut.fel.aos.airline.jaxrs.exceptions.AuthException;
import cz.cvut.fel.aos.airline.jaxrs.exceptions.ReservationNotFoundException;
import cz.cvut.fel.aos.airline.jaxrs.service.AuthService;
import cz.cvut.fel.aos.airline.jaxrs.service.ReservationRepository;
import cz.cvut.fel.aos.airline.model.User;
import org.apache.commons.codec.binary.Base64;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class ReservationFilter implements javax.servlet.Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if(request.getMethod().equals("GET") || request.getMethod().equals("DELETE") || request.getMethod().equals("PUT")){

            String passHeader = request.getHeader("X-password");
            if(passHeader==null) passHeader=request.getParameter("X-password");


            if(passHeader==(null) || passHeader.equals("")) {
                String authHeader = request.getHeader("Authorization");
                if (authHeader != null) {
                    StringTokenizer st = new StringTokenizer(authHeader);
                    if (st.hasMoreTokens()) {
                        String basic = st.nextToken();

                        if (basic.equalsIgnoreCase("Basic")) {
                            try {
                                String credentials = new String(Base64.decodeBase64(st.nextToken().getBytes()), "UTF-8");
                                int p = credentials.indexOf(":");
                                if (p != -1) {
                                    String _username = credentials.substring(0, p).trim();
                                    String _password = credentials.substring(p + 1).trim();

                                    AuthService authService = new AuthService();
                                    User user = authService.authenticateUser(_username, _password);

                                    if (!user.getRole().equals(User.Role.ADMIN) && !user.getRole().equals(User.Role.MANAGER)) unauthorized(response, "Unauthorised");
                                    else {
                                        filterChain.doFilter(new UserRoleRequestWrapper(user, user.getRole().toString(), request), servletResponse);
                                    }
                                } else {
                                    unauthorized(response, "Invalid authentication token");
                                }
                            } catch (UnsupportedEncodingException e) {
                                throw new Error("Couldn't retrieve authentication", e);
                            } catch (AuthException e) {
                                unauthorized(response, "Bad credentials");
                            }
                        }
                    }
                } else {
                    unauthorized(response, "Unauthorised");
                }
            }else{
                String path=request.getRequestURI();
                Map<String, String> map=new HashMap<>();
                UriTemplate template=new UriTemplate("/{prefix}/service/reservation/{id}");
                Long id;
                if(template.match(path, map)) {
                    id = Long.parseLong(map.get("id"));
                }else{
                    template=new UriTemplate("/{prefix}/service/reservation/{id}/{method}");
                    template.match(path, map);
                    id = Long.parseLong(map.get("id"));
                }
                try {
                    if(ReservationRepository.getReservationById(id).getPassword().getPassword().equals(passHeader))
                    filterChain.doFilter(servletRequest,servletResponse);
                    else response.sendError(401, "Unauthorised");
                } catch (ReservationNotFoundException e) {
                    e.printStackTrace();
                    response.sendError(401, "Unauthorised");
                }
            }

        }else filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
    }

    private void unauthorized(HttpServletResponse response, String message) throws IOException {
        response.setHeader("WWW-Authenticate", "Basic realm=\"file\"");
        response.sendError(401, message);
    }

}