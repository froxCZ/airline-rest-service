package cz.cvut.fel.aos.airline.jaxrs.service;

import cz.cvut.fel.aos.airline.jaxrs.exceptions.DestionationNotFoundExeption;
import cz.cvut.fel.aos.airline.model.destination.Destination;

import java.util.*;

/**
 * Created by frox on 19.10.15.
 */
public class DestinationRepository {
    private static final Map<Long, Destination> DESTINATIONS = new HashMap<>();
    private static int destinationIdInc;

    static {
        addDestination(new Destination("Prague", 50.101502, 14.263095));
        addDestination(new Destination("London", 51.470056, -0.454467));
    }


    public static List<Destination> getAllDestinations(DestinationFilter destinationFilter) {
        ArrayList<Destination> destinationsBeforeFilter = new ArrayList<>(DESTINATIONS.values());
        ArrayList<Destination> destinationsAfterFilter = new ArrayList<>(DESTINATIONS.values());
                if (destinationFilter.getOrderingHow() == DestinationFilter.ORDERING_HOW.ASC) {
                    Collections.sort(destinationsAfterFilter, new Destination.NameComparatorAsc());
                } else {
                    Collections.sort(destinationsAfterFilter, new Destination.NameComparatorDesc());
                }

        return destinationsAfterFilter;
    }

    public static Destination getDestinationById(long id) {
        return DESTINATIONS.get(id);
    }

    public static int getDestinationCount() {
        return DESTINATIONS.size();
    }

    public static synchronized void addDestination(Destination destination) {
        destination.setId(destinationIdInc++);
        DESTINATIONS.put(destination.getId(), destination);
    }

    public static void updateDestination(long destinationId, Destination destination) throws DestionationNotFoundExeption {
        if(DESTINATIONS.containsKey(destinationId)) {
            destination.setId(destinationId);
            DESTINATIONS.put(destination.getId(), destination);
        }else{
            throw new DestionationNotFoundExeption();
        }
    }

    public static void deleteDestination(long destinationId) {
        DESTINATIONS.remove(destinationId);
        FlightRepository.deleteFlightsWithDestination(destinationId);
    }



    public static class DestinationFilter {
        private ORDERING_HOW orderingHow = ORDERING_HOW.ASC;
        public enum ORDERING_HOW {ASC, DESC}


        public DestinationFilter() {
        }

        public ORDERING_HOW getOrderingHow() {
            return orderingHow;
        }
        public void setOrderingHow(ORDERING_HOW orderingHow) {
            this.orderingHow = orderingHow;
        }
    }
}
