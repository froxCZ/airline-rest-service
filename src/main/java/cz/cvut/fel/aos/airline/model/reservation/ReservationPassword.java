package cz.cvut.fel.aos.airline.model.reservation;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Jan on 09.11.2015.
 */
@XmlRootElement
public class ReservationPassword{
    private String password;
    private long id;

    public ReservationPassword() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ReservationPassword(String password, Long id) {
        this.password = password;
        this.id=id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
