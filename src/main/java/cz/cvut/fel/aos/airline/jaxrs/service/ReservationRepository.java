package cz.cvut.fel.aos.airline.jaxrs.service;

import cz.cvut.fel.aos.airline.jaxrs.exceptions.*;
import cz.cvut.fel.aos.airline.model.flight.Flight;
import cz.cvut.fel.aos.airline.model.reservation.Reservation;
import cz.cvut.fel.aos.airline.model.State;
import cz.cvut.fel.aos.airline.model.reservation.ReservationPassword;

import java.util.*;

public class ReservationRepository {
    private static final Map<Long, Reservation> RESERVATIONS = new HashMap<>();
    private static Long reservationIdInc=new Long(0);

    static {
        try {
            addReservation(new Reservation(new Long(5),new Long(1),new ReservationPassword("helloworld",0l),1));
        } catch (FlightFullException e) {
            e.printStackTrace();
        } catch (FlightRepository.FlightNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static Long getReservationIdInc() {
        return reservationIdInc;
    }

    public static void setReservationIdInc(Long reservationIdInc) {
        ReservationRepository.reservationIdInc = reservationIdInc;
    }

    public static List<Reservation> getAllReservations() {
        return new ArrayList<>(RESERVATIONS.values());
    }

    public static Reservation getReservationById(long id) throws ReservationNotFoundException {

        if (!RESERVATIONS.containsKey(id)) throw new ReservationNotFoundException();
        return RESERVATIONS.get(id);
    }

    public static int getReservationCount() {
        return RESERVATIONS.size();
    }

    public static synchronized Reservation addReservation(Reservation reservation) throws FlightFullException, FlightRepository.FlightNotFoundException {
        if (FlightRepository.getFlightById(reservation.getFlight())==null) throw new FlightRepository.FlightNotFoundException();
        if(!FlightRepository.getFlightById(reservation.getFlight()).reserveSeats(reservation.getSeats())) throw new FlightFullException();
        reservation.setId(reservationIdInc++);
        reservation.setState(State.NEW);
        reservation.setCreated(new Date());
        RESERVATIONS.put(reservation.getId(), reservation);
        return reservation;
    }



    public static void cancelReservation(Long reservationId) throws ReservationNotFoundException, ReservationNotNewException {
        if(RESERVATIONS.containsKey(reservationId)) {
            Reservation res = RESERVATIONS.get(reservationId);
            if(res.getState()==State.NEW){
            res.setState(State.CANCELED);
            FlightRepository.getFlightById(res.getFlight()).releaseSeats(res.getSeats());}
            else{
                throw new ReservationNotNewException();
            }
        }else{
            throw new ReservationNotFoundException();
        }
    }

    public static void payReservation(Long reservationId) throws ReservationNotFoundException, ReservationCanceledException, ReservationPaidException {
        if(RESERVATIONS.containsKey(reservationId)) {
            Reservation res=RESERVATIONS.get(reservationId);
            if(res.getState()==State.PAID)throw new ReservationPaidException();
            if(res.getState()==State.CANCELED)throw new ReservationCanceledException();
            res.setState(State.PAID);
        }else{
            throw new ReservationNotFoundException();
        }
    }

    public static void deleteReservation(Long reservationId) throws ReservationPaidException, ReservationNotFoundException {
        if(RESERVATIONS.containsKey(reservationId)) {
        Reservation res = RESERVATIONS.get(reservationId);

        if (res.getState()!= State.PAID) {
            res.setState(State.CANCELED);
            FlightRepository.getFlightById(res.getFlight()).releaseSeats(res.getSeats());
            RESERVATIONS.remove(reservationId);
        }else {
            throw new ReservationPaidException();
        }}
        else{
            throw new ReservationNotFoundException();
        }
    }

    public static void deleteReservationWithFLight(Long flighId) {
        Iterator<Reservation> iterator = RESERVATIONS.values().iterator();
        while (iterator.hasNext()) {
            Reservation reservation = iterator.next();
            if (reservation.getFlight() == flighId) {
                if (reservation.getState()!= State.PAID) {
                    iterator.remove();
                }else {
                    iterator.remove();/*?reservace je zaplacena ale stejne ji smažu?*/
                }
            }
        }
    }

}
