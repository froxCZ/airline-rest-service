package cz.cvut.fel.aos.airline.filters;

/**
 * Created by Jan on 15.12.2015.
 */
import cz.cvut.fel.aos.airline.model.User;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;


public class UserRoleRequestWrapper extends HttpServletRequestWrapper {


    User user;
    String role = null;
    HttpServletRequest realRequest;

    public UserRoleRequestWrapper(User user, String role, HttpServletRequest request) {
        super(request);
        this.user = user;
        this.role = role;
        this.realRequest = request;
    }

    @Override
    public boolean isUserInRole(String role) {
        if (role == null) {
            return this.realRequest.isUserInRole(role);
        }
        return this.role.equals(role);
    }

    @Override
    public Principal getUserPrincipal() {
        if (this.user == null) {
            return realRequest.getUserPrincipal();
        }

        return new Principal() {
            @Override
            public String getName() {
                return user.getName();
            }
        };
    }
}
