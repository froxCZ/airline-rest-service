package cz.cvut.fel.aos.airline.filters;

/**
 * Created by Jan on 15.12.2015.
 */

import cz.cvut.fel.aos.airline.jaxrs.exceptions.AuthException;
import cz.cvut.fel.aos.airline.jaxrs.service.AuthService;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import cz.cvut.fel.aos.airline.model.User;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import javax.servlet.http.HttpServletResponse;

public class AuthFilter implements javax.servlet.Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if(request.getMethod().equals("POST") || request.getMethod().equals("DELETE") || request.getMethod().equals("PUT")) {
            String authHeader = request.getHeader("Authorization");
            if (authHeader != null) {
                StringTokenizer st = new StringTokenizer(authHeader);
                if (st.hasMoreTokens()) {
                    String basic = st.nextToken();

                    if (basic.equalsIgnoreCase("Basic")) {
                        try {
                            String credentials = new String(Base64.decodeBase64(st.nextToken().getBytes()), "UTF-8");
                            int p = credentials.indexOf(":");
                            if (p != -1) {
                                String _username = credentials.substring(0, p).trim();
                                String _password = credentials.substring(p + 1).trim();

                                AuthService authService = new AuthService();
                                User user = authService.authenticateUser(_username, _password);

                                if (!user.getRole().equals(User.Role.ADMIN)){
                                    unauthorized(response, "Unauthorised");
                                }else {
                                    filterChain.doFilter(new UserRoleRequestWrapper(user, user.getRole().toString(), request), servletResponse);
                                }
                            } else {
                                unauthorized(response, "Unauthorised");
                            }
                        } catch (UnsupportedEncodingException e) {
                            throw new Error("Couldn't retrieve authentication", e);
                        } catch (AuthException e) {
                            unauthorized(response, "Unauthorised");
                        }
                    }
                }
            }
            else{unauthorized(response, "Unauthorised");}
        }
        else{
            filterChain.doFilter(servletRequest,servletResponse);}
    }

    @Override
    public void destroy() {
    }

    private void unauthorized(HttpServletResponse response, String message) throws IOException {
        response.setHeader("WWW-Authenticate", "Basic realm=\"file\"");
        response.sendError(401, message);
    }

}