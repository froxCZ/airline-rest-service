package cz.cvut.fel.aos.airline.jaxrs.service.rome2rio;

import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.Location;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Jan on 14.12.2015.
 */
@XmlRootElement
public class Segment {
    String kind;
    String vehicle;
    Boolean isMajor;
    Boolean isImperial;
    Float distance;
    Float duration;
    String sName;
    Location sPos;
    String tName;
    Location tPos;
    IndicativePrice indicativePrice;
    Path path;
    String itineraries;

    public Segment() {
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public Boolean getIsMajor() {
        return isMajor;
    }

    public void setIsMajor(Boolean isMajor) {
        this.isMajor = isMajor;
    }

    public Boolean getIsImperial() {
        return isImperial;
    }

    public void setIsImperial(Boolean isImperial) {
        this.isImperial = isImperial;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public Location getsPos() {
        return sPos;
    }

    public void setsPos(Location sPos) {
        this.sPos = sPos;
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    public Location gettPos() {
        return tPos;
    }

    public void settPos(Location tPos) {
        this.tPos = tPos;
    }

    public IndicativePrice getIndicativePrice() {
        return indicativePrice;
    }

    public void setIndicativePrice(IndicativePrice indicativePrice) {
        this.indicativePrice = indicativePrice;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public String getItineraries() {
        return itineraries;
    }

    public void setItineraries(String itineraries) {
        this.itineraries = itineraries;
    }
}
