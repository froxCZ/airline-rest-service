package cz.cvut.fel.aos.airline.jaxrs.service.rome2rio;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Jan on 14.12.2015.
 */
@XmlRootElement
public class Rome2RioResponse {
    private List<Route> routes;
    private List<Place> places;
    private List<String> airports;
    private List<String> airlines;
    private List<String> aircraft;
    private List<String> agency;
    private Integer serveTime;
private String status;

    public Rome2RioResponse() {
    }



    public Float getDistance(){

            if(routes==null){
                return (float)(routes.get(0).getDistance());
            } else {
                //exception
                return 0.0f;
            }

    }

    @Override
    public String toString() {
        return "Rome2RioResponse{" +
                "routes=" + routes +
                ", status='" + status + '\'' +
                '}';
    }
}
