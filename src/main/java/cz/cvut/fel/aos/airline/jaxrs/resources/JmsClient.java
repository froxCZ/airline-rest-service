package cz.cvut.fel.aos.airline.jaxrs.resources;

import com.sun.org.apache.regexp.internal.RE;
import cz.cvut.fel.aos.airline.jaxrs.exceptions.DestionationNotFoundExeption;
import cz.cvut.fel.aos.airline.jaxrs.service.DestinationRepository;
import cz.cvut.fel.aos.airline.jaxrs.service.FlightRepository;
import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.GeocodeService;
import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.Location;
import cz.cvut.fel.aos.airline.jms.JmsReceiver;
import cz.cvut.fel.aos.airline.jms.MessageWrapper;
import cz.cvut.fel.aos.airline.model.destination.Destination;
import cz.cvut.fel.aos.airline.model.destination.DestinationDTO;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by frox on 22.10.15.
 */
@Stateless
@Path(JmsClient.PATH)
public class JmsClient {
    public static final String PATH = "service/JMS";

    private static final long serialVersionUID = -1;

    private static final int MSG_COUNT = 5;

    private final static Logger LOGGER = Logger.getLogger(JmsClient.class.toString());

    @Resource(mappedName = "myQueueConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "myQueue")
    private Queue queue;

    @GET
    public Response testJMS() throws Exception {

        Connection connection = null;
        try {
            javax.jms.Destination destination;

            destination = queue;


            LOGGER.info(connectionFactory+"");
            connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(destination);

            // settings
            messageProducer.setDeliveryMode(DeliveryMode.PERSISTENT);
            // PERSISTENT = This delivery mode instructs the JMS provider to log the message to stable storage as part of the client's send operation.
            // NON_PERSISTENT = This is the lowest-overhead delivery mode because it does not require that the message be logged to stable storage.
            messageProducer.setTimeToLive(10000); // miliseconds to live

            connection.start();


            // send text message
            TextMessage message = session.createTextMessage();
            for (int i = 0; i < MSG_COUNT; i++) {
                message.setText("This is TEXT message " + (i + 1));
                messageProducer.send(message);
            }

            // send object message
            ObjectMessage objectMessage = session.createObjectMessage();
            for (int i = 0; i < MSG_COUNT; i++) {
                objectMessage.setObject(new MessageWrapper("This is OBJECT message " + (i + 1)));
                messageProducer.send(objectMessage);
            }


        } catch (JMSException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        }
        return Response.status(200).build();
    }

    @GET
    @Path("/all")
    public String getAll() throws Exception {
        ArrayList<String> messages= JmsReceiver.txtmessages;
        String ret="";
        for (int i=0; i<messages.size();i++){
            ret+=messages.get(i)+"<br>";
        }
        return ret;
    }

}
