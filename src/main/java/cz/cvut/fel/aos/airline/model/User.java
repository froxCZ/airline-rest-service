package cz.cvut.fel.aos.airline.model;

/**
 * Created by frox on 25.10.15.
 */
public class User {
    public enum Role {
        BASIC, MANAGER, ADMIN;
    }

    private String name, password;
    private Role role;

    public User(String name, String password, Role role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
