package cz.cvut.fel.aos.airline.jaxrs.resources;

import cz.cvut.fel.aos.airline.jaxrs.service.DestinationRepository;
import cz.cvut.fel.aos.airline.jaxrs.service.FlightRepository;
import cz.cvut.fel.aos.airline.jaxrs.service.rome2rio.Rome2Rio;
import cz.cvut.fel.aos.airline.model.flight.Flight;
import cz.cvut.fel.aos.airline.model.flight.FlightDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by frox on 22.10.15.
 */
@Path(FlightResource.PATH)
public class FlightResource {
    @Context
    UriInfo uriInfo;
    public static final String PATH = "service/flight/";
    private static final String HEADER_FILTER_DATE_DEPARTURE_FROM = "dateOfDepartureFrom=";
    private static final String HEADER_FILTER_DATE_DEPARTURE_TO = "dateOfDepartureTo=";
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<FlightDTO> getAllFlights(@HeaderParam("X-Base") Integer base, @HeaderParam("X-Offset") Integer offset, @HeaderParam("X-Filter") String filterQuery, @HeaderParam("X-Order") String ordering) throws Exception {
        Date dateOfDepartureFrom = null, dateOfDepartureTo = null;
        if (filterQuery != null) {
            String[] params = filterQuery.split(",");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");//ignoring timezone.. +01:00 not supported by simpledate format..
            for (String s : params) {
                int index = s.indexOf("=");
                if (index <= 0) continue;
                Date date = dateFormat.parse(s.substring(index + 1));
                if (s.substring(0, index + 1).equals(HEADER_FILTER_DATE_DEPARTURE_FROM)) {
                    dateOfDepartureFrom = date;
                } else if (s.substring(0, index + 1).equals(HEADER_FILTER_DATE_DEPARTURE_TO)) {
                    dateOfDepartureTo = date;
                }

            }

        }
        FlightRepository.FlightFilter flightFilter = new FlightRepository.FlightFilter();
        flightFilter.setBase(base);
        flightFilter.setOffset(offset);
        flightFilter.setDateOfDepartureFrom(dateOfDepartureFrom);
        flightFilter.setDateOfDepartureTo(dateOfDepartureTo);
        if (ordering != null) {
            String[] orderingSplitted = ordering.split(":");
            String orderBy = orderingSplitted[0], orderHow = orderingSplitted[1];
            if (orderingSplitted.length == 2) {
                if (orderBy.equals("dateOfDeparture")) {
                    flightFilter.setOrderingBy(FlightRepository.FlightFilter.ORDERING_BY.DEPARTURE_DATE);
                } else if (orderBy.equals("name")) {
                    flightFilter.setOrderingBy(FlightRepository.FlightFilter.ORDERING_BY.NAME);
                }
                if (orderHow.equals("asc")) {
                    flightFilter.setOrderingHow(FlightRepository.FlightFilter.ORDERING_HOW.ASC);
                } else if (orderHow.equals("desc")) {
                    flightFilter.setOrderingHow(FlightRepository.FlightFilter.ORDERING_HOW.DESC);
                }
            }
        }
        return FlightDTO.createDTOList(FlightRepository.getFlights(flightFilter),uriInfo);
    }

    @GET
    @Path("{flightId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public FlightDTO getFlightById(@PathParam("flightId") int flightId) throws Exception {
        return new FlightDTO(FlightRepository.getFlightById(flightId), uriInfo);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response saveFlight(FlightDTO flightDTO) {
        Float distance= null;
        try {
            distance = Rome2Rio.getDistance(DestinationRepository.getDestinationById(flightDTO.getFrom()).getName(), DestinationRepository.getDestinationById(flightDTO.getTo()).getName());
        } catch (UnsupportedEncodingException e) {
            Response.status(403).build();
        }
        if(distance!=null){
            Float price=distance*10;
            flightDTO.setPrice(price);
        }
        flightDTO.setDistance(distance);
        FlightRepository.addFlight(new Flight(flightDTO));
        return Response.status(200).build();
    }

    @PUT
    @Path("{flightId}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateFlight(@PathParam("flightId") int flightId, FlightDTO flightDTO) {
        try {
            FlightRepository.updateFlight(flightId, flightDTO);
            return Response.status(200).build();
        } catch (FlightRepository.FlightNotFoundException e) {
            return Response.status(404).build();
        }
    }

    @DELETE
    @Path("{flightId}")
    public Response deleteFlight(@PathParam("flightId") long flightId) {
        FlightRepository.deleteFlight(flightId);
        return Response.status(200).build();
    }

}
