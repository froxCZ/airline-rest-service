package cz.cvut.fel.aos.airline.model.reservation;

import cz.cvut.fel.aos.airline.model.State;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Jan on 09.11.2015.
 */
@XmlRootElement
public class ReservationDTO {
    protected Long  id;
    protected Long  flight; //Flight id
    protected String created;
    protected int seats;
    protected State state;

    protected String flightUrl, url;

    public ReservationDTO() {
    }

    public ReservationDTO(Reservation reservation,UriInfo uriInfo) {
        id=reservation.getId();
        url= uriInfo.getBaseUriBuilder().path("service/reservation").build().getPath()+"/"+id;

        flight=reservation.getFlight();
        flightUrl=uriInfo.getBaseUriBuilder().path("service/flight").build().getPath()+"/"+flight;

        state=reservation.getState();
        seats=reservation.getSeats();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        df.setTimeZone(TimeZone.getTimeZone("Europe/Prague"));
        created=df.format(reservation.getCreated());


    }

    public static List<ReservationDTO> createDTOList(List<Reservation> reservationList, UriInfo uriInfo) {
        LinkedList<ReservationDTO> reservationDTOs = new LinkedList<>();
        for (Reservation reservation : reservationList) {
            reservationDTOs.add(new ReservationDTO(reservation, uriInfo));
        }
        return reservationDTOs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFlight() {
        return flight;
    }

    public void setFlight(Long flight) {
        this.flight = flight;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getFlightUrl() {
        return flightUrl;
    }

    public void setFlightUrl(String flightUrl) {
        this.flightUrl = flightUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
