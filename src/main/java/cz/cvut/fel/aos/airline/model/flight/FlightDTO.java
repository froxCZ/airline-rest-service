package cz.cvut.fel.aos.airline.model.flight;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by frox on 7.11.15.
 */
@XmlRootElement
public class FlightDTO{
    protected String name;
    protected long id;
    protected String dateOfDeparture;
    protected Float price;
    protected Float distance;

    protected int from, to, seats;

    protected String fromUrl, toUrl, url;

    public FlightDTO(Flight flight, UriInfo uriInfo) {
        id = flight.getId();
        from = flight.getFrom();
        to = flight.getTo();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        df.setTimeZone(TimeZone.getTimeZone("Europe/Prague"));
        dateOfDeparture = df.format(flight.getDateOfDeparture());

        name = flight.getName();
        seats = flight.getSeats();
        price = flight.getPrice();
        distance = flight.getDistance();
        fromUrl = uriInfo.getBaseUriBuilder().path("service/destination").build().getPath() + "/" + getFrom();
        toUrl = uriInfo.getBaseUriBuilder().path("service/destination").build().getPath() + "/" + getTo();
        url = uriInfo.getBaseUriBuilder().path("service/flight").build().getPath() + "/" + getId();
    }

    public FlightDTO() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFromUrl() {
        return fromUrl;
    }

    public void setFromUrl(String fromUrl) {
        this.fromUrl = fromUrl;
    }

    public String getToUrl() {
        return toUrl;
    }

    public void setToUrl(String toUrl) {
        this.toUrl = toUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDateOfDeparture() {
        return dateOfDeparture;
    }

    public void setDateOfDeparture(String dateOfDeparture) {
        this.dateOfDeparture = dateOfDeparture;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public static List<FlightDTO> createDTOList(List<Flight> flightList, UriInfo uriInfo) {
        LinkedList<FlightDTO> flightDTOs = new LinkedList<>();
        for (Flight flight : flightList) {
            flightDTOs.add(new FlightDTO(flight, uriInfo));
        }
        return flightDTOs;
    }
}
