package cz.cvut.fel.aos.airline.model;


import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class used to store payment information.
 * For now its just a random number.
 */
@XmlRootElement
public class Payment{
    private int cardNumber;
    /**
     * Class used as XML must have empty constructor! Otherwise 500 error with no message is thrown!
     */
    public Payment() {
    }

    public Payment(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }
}
