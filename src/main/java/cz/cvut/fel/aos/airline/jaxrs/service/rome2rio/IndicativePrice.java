package cz.cvut.fel.aos.airline.jaxrs.service.rome2rio;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Jan on 14.12.2015.
 */
@XmlRootElement
public class IndicativePrice {
Float price;
    String currency;
    Float nativePrice;
    String nativeCurrency;
    Boolean isFreeTransfer;

    public IndicativePrice() {
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Float getNativePrice() {
        return nativePrice;
    }

    public void setNativePrice(Float nativePrice) {
        this.nativePrice = nativePrice;
    }

    public String getNativeCurrency() {
        return nativeCurrency;
    }

    public void setNativeCurrency(String nativeCurrency) {
        this.nativeCurrency = nativeCurrency;
    }

    public Boolean getIsFreeTransfer() {
        return isFreeTransfer;
    }

    public void setIsFreeTransfer(Boolean isFreeTransfer) {
        this.isFreeTransfer = isFreeTransfer;
    }
}
