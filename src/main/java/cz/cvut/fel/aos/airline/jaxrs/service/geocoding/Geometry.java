package cz.cvut.fel.aos.airline.jaxrs.service.geocoding;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by frox on 12.12.15.
 */
@XmlRootElement
public class Geometry {
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
