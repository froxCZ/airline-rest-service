package cz.cvut.fel.aos.airline.jaxrs.service;

import cz.cvut.fel.aos.airline.jaxrs.exceptions.AuthException;
import cz.cvut.fel.aos.airline.model.User;

import java.util.HashMap;

/**
 * Created by frox on 25.10.15.
 */
public class AuthService {
    static HashMap<String, User> USERS = new HashMap<>();

    static {
        USERS.put("admin", new User("admin", "admin", User.Role.ADMIN));
        USERS.put("franta", new User("franta", "franta", User.Role.BASIC));
        USERS.put("pepa", new User("pepa", "pepa", User.Role.MANAGER));
    }

    /**
     * returns authenticated user or exception
     * @param userName
     * @param password
     * @return
     * @throws AuthException
     */
    public User authenticateUser(String userName, String password) throws AuthException {
        User user = USERS.get(userName);
        if (user == null || user.getPassword() == null || !user.getPassword().equals(password)) {
            throw new AuthException();
        } else {
            return user;
        }
    }
}
