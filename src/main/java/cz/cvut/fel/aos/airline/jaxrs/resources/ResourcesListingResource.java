/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.aos.airline.jaxrs.resources;


import com.sun.jersey.api.view.Viewable;
import cz.cvut.fel.aos.airline.model.Resources;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URISyntaxException;


@Path("/")
public class ResourcesListingResource {

    @Context
    UriInfo uriInfo;
    
    @GET
    @Produces({ MediaType.TEXT_HTML })
    public Response webPage() throws URISyntaxException {
        return Response.ok(new Viewable("/index"), MediaType.TEXT_HTML)
                .build();
    }
    
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Resources resources() {
        return new Resources(uriInfo);
    }
}
