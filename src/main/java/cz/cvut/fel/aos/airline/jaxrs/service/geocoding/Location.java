package cz.cvut.fel.aos.airline.jaxrs.service.geocoding;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by frox on 12.12.15.
 */
@XmlRootElement
public class Location {
    private double lat, lng;

    public Location() {
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Location{" +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
