package cz.cvut.fel.aos.airline.model;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Jan on 10.11.2015.
 */
@XmlRootElement
public class Resources {


    protected String flightUrl, destinationUrl, reservationUrl;

    public Resources(UriInfo uriInfo) {
        flightUrl= uriInfo.getAbsolutePathBuilder().path("service/flight").build().getPath();
        destinationUrl = uriInfo.getAbsolutePathBuilder().path("service/destination").build().getPath();
        reservationUrl = uriInfo.getAbsolutePathBuilder().path("service/reservation").build().getPath();
    }

    public String getFlightUrl() {
        return flightUrl;
    }

    public void setFlightUrl(String flightUrl) {
        this.flightUrl = flightUrl;
    }

    public String getDestinationUrl() {
        return destinationUrl;
    }

    public void setDestinationUrl(String destinationUrl) {
        this.destinationUrl = destinationUrl;
    }

    public String getReservationUrl() {
        return reservationUrl;
    }

    public void setReservationUrl(String reservationUrl) {
        this.reservationUrl = reservationUrl;
    }
}
