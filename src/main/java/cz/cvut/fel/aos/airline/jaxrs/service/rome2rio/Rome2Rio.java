package cz.cvut.fel.aos.airline.jaxrs.service.rome2rio;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.GeocodeResponse;
import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.Location;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by frox on 12.12.15.
 */
public class Rome2Rio {

    /**
     * returns Lenght of flight or zeo
     * @param from
     * @param to
     * @return
     */
    public static Float getDistance(String from, String to) throws UnsupportedEncodingException {
        Client client = Client.create();

        WebResource webResource;
        webResource = client.resource("http://free.rome2rio.com/api/1.2/json/Search?key=" + "b22pmi9y&oName=" + URLEncoder.encode(from, "UTF-8")+"&dName="+URLEncoder.encode(to, "UTF-8")+"&flags=0x000FFFF0" );
        ClientResponse response = webResource.accept("application/json")
                    .get(ClientResponse.class);
            if (response.getStatus() == 200) {
               response.bufferEntity();
                 JSONObject rome2RioResponse = response.getEntity(JSONObject.class);
                try {
                    JSONArray routes=rome2RioResponse.getJSONArray("routes");
                    Float distance= Float.parseFloat(routes.getJSONObject(0).getString("distance"));
                    return distance;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        return null;
    }
}
