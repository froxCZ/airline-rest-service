package cz.cvut.fel.aos.airline.jaxrs.service.email.ws;

import cz.cvut.fel.aos.airline.jms.JmsReceiver;

import javax.ejb.Stateless;
import javax.jms.*;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.logging.Logger;

/**
 * Created by Jan on 08.01.2016.
 */
@Stateless
@WebService(
        name = "sendReservationService",
        serviceName = "sendReservationService",
        portName = "sendReservationPort",
        targetNamespace = "email",
        wsdlLocation = "email.wsdl")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
@XmlSeeAlso({ObjectFactory.class})
public class SendReservationService {


    private static final long serialVersionUID = -1;

    private final static Logger LOGGER = Logger.getLogger(SendReservationService.class.toString());

    //@Resource(mappedName = "myQueueConnectionFactory")
    private ConnectionFactory connectionFactory;

    //@Resource(mappedName = "myQueue")
    private Queue queue;

    public SendReservationService() {
        try {
            connectionFactory = (ConnectionFactory) new InitialContext().lookup("myQueueConnectionFactory");
            queue = (Queue) new InitialContext().lookup("myQueue");
        } catch (NamingException ex) {
            ex.printStackTrace();
        }
    }


    @WebMethod(operationName = "sendReservationEmail")
    public void sendReservationEmail(
            @WebParam(name = "Email", partName = "email")
            String email, @WebParam(name = "Reservation", partName = "reservation")
    String generatedFile) {
        Connection connection = null;
        try {
            javax.jms.Destination destination;

            destination = queue;


            LOGGER.info(connectionFactory + "");
            connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(destination);

            // settings
            messageProducer.setDeliveryMode(DeliveryMode.PERSISTENT);
            // PERSISTENT = This delivery mode instructs the JMS provider to log the message to stable storage as part of the client's send operation.
            // NON_PERSISTENT = This is the lowest-overhead delivery mode because it does not require that the message be logged to stable storage.
            messageProducer.setTimeToLive(10000); // miliseconds to live

            connection.start();


            // send text message
            BytesMessage bytesMessage = session.createBytesMessage();
            bytesMessage.writeBytes(generatedFile.getBytes());
            bytesMessage.setStringProperty(JmsReceiver.EMAIL_MSG_RECIPIENT_KEY, email);
            bytesMessage.setStringProperty(JmsReceiver.EMAIL_MSG_CONTENT_KEY, "Dear customer, your e-ticket is attached.");
            messageProducer.send(bytesMessage);


        } catch (JMSException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        }
        return;

    }

}


