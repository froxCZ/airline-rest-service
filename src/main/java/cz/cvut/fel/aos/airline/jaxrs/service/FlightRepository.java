package cz.cvut.fel.aos.airline.jaxrs.service;

import cz.cvut.fel.aos.airline.helper.MyUtil;
import cz.cvut.fel.aos.airline.model.flight.Flight;
import cz.cvut.fel.aos.airline.model.flight.FlightDTO;

import java.util.*;


/**
 * Created by frox on 19.10.15.
 */
public class FlightRepository {
    private static final Map<Long, Flight> FLIGHTS = new HashMap<>();
    private static long flightIncId;

    static {
        addFlight(new Flight("F1", MyUtil.createDate(2014, 6, 7, 10, 5, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F2", MyUtil.createDate(2014, 9, 10, 18, 20, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F3", MyUtil.createDate(2014, 9, 10, 15, 5, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F4", MyUtil.createDate(2014, 1, 3, 10, 25, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F5", MyUtil.createDate(2014, 1, 7, 15, 50, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F6", MyUtil.createDate(2015, 1, 1, 10, 55, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F7", MyUtil.createDate(2015, 10, 1, 11, 0, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F8", MyUtil.createDate(2015, 10, 8, 13, 55, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F9", MyUtil.createDate(2015, 5, 1, 11, 40, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F10", MyUtil.createDate(2014, 12, 10, 9, 15, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F11", MyUtil.createDate(2014, 1, 1, 11, 0, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F12", MyUtil.createDate(2014, 2, 1, 11, 20, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F13", MyUtil.createDate(2014, 3, 1, 11, 25, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F14", MyUtil.createDate(2014, 4, 1, 11, 20, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F15", MyUtil.createDate(2014, 10, 2, 20, 35, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F16", MyUtil.createDate(2014, 10, 3, 21, 30, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F17", MyUtil.createDate(2014, 10, 4, 15, 45, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F18", MyUtil.createDate(2014, 10, 5, 14, 50, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F19", MyUtil.createDate(2014, 10, 6, 17, 45, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F20", MyUtil.createDate(2014, 10, 7, 10, 30, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F21", MyUtil.createDate(2014, 10, 8, 6, 20, 0), 10584.0f, 1487.0f, 0, 1, 150));
        addFlight(new Flight("F22", MyUtil.createDate(2014, 10, 9, 7, 15, 0), 10584.0f, 1487.0f, 0, 1, 150));
    }

    public static List<Flight> getFlights(FlightFilter flightFilter) {
        List<Flight> flights = new ArrayList<>(FLIGHTS.values());
        List<Flight> flightsAfterFilter = new ArrayList<>();
        if (flightFilter.getDateOfDepartureFrom() == null && flightFilter.getDateOfDepartureTo() == null) {
            flightsAfterFilter = flights;
        } else {
            for (Flight flight : flights) {
                if ((flightFilter.getDateOfDepartureFrom() == null || flight.getDateOfDeparture().after(flightFilter.getDateOfDepartureFrom())) &&
                        (flightFilter.getDateOfDepartureTo() == null || flight.getDateOfDeparture().before(flightFilter.getDateOfDepartureTo()))) {
                    flightsAfterFilter.add(flight);
                }
            }
        }
        flights = null;
        if (flightFilter.getBase() != null || flightFilter.getOffset() != null) {
            int lastIndex,firstIndex;
            if(flightFilter.getBase() == null){
                firstIndex = 0;
            }else{
                firstIndex = flightFilter.getBase();
            }
            if (flightFilter.getOffset() == null) {
                lastIndex = flightsAfterFilter.size() - 1;
            } else {
                lastIndex = firstIndex + flightFilter.getOffset();
                if (lastIndex >= flightsAfterFilter.size()) {
                    lastIndex = flightsAfterFilter.size() - 1;
                }
            }
            flightsAfterFilter = flightsAfterFilter.subList(firstIndex, lastIndex);
        }
        if (flightFilter.getOrderingBy() != FlightFilter.ORDERING_BY.NONE) {
            if (flightFilter.getOrderingBy() == FlightFilter.ORDERING_BY.NAME) {
                if (flightFilter.getOrderingHow() == FlightFilter.ORDERING_HOW.ASC) {
                    Collections.sort(flightsAfterFilter, new Flight.NameComparatorAsc());
                } else {
                    Collections.sort(flightsAfterFilter, new Flight.NameComparatorDesc());
                }
            } else if (flightFilter.getOrderingBy() == FlightFilter.ORDERING_BY.DEPARTURE_DATE) {
                if (flightFilter.getOrderingHow() == FlightFilter.ORDERING_HOW.ASC) {
                    Collections.sort(flightsAfterFilter, new Flight.DepartureDateComparatorAsc());
                } else {
                    Collections.sort(flightsAfterFilter, new Flight.DepartureDateComparatorDesc());
                }
            }
        }
        return flightsAfterFilter;

    }

    public static Flight getFlightById(long id) {
        return FLIGHTS.get(id);
    }

    public static int getFlightCount() {
        return FLIGHTS.size();
    }

    public static synchronized void addFlight(Flight flight) {
        flight.setId(flightIncId++);
        FLIGHTS.put(flight.getId(), flight);
    }

    public static void updateFlight(long flightId, FlightDTO flightDTO) throws FlightNotFoundException {
        Flight flight = FLIGHTS.get(flightId);
        if (flight != null) {
            flight.updateFromDTO(flightDTO);
        } else {
            throw new FlightNotFoundException();
        }
    }

    public static void deleteFlight(long flightId) {
        FLIGHTS.remove(flightId);
    }

    public static void deleteFlightsWithDestination(long destinationId) {
        Iterator<Flight> iterator = FLIGHTS.values().iterator();
        while (iterator.hasNext()) {
            Flight flight = iterator.next();
            if (flight.getFrom() == destinationId || flight.getTo() == destinationId) {
                long flightId=flight.getId();
                ReservationRepository.deleteReservationWithFLight(flightId);
                iterator.remove();
            }
        }
    }


    public static class FlightNotFoundException extends Throwable {
    }

    public static class FlightFilter {
        private Integer base, offset;
        private Date dateOfDepartureFrom, dateOfDepartureTo;
        private ORDERING_BY orderingBy = ORDERING_BY.NONE;
        private ORDERING_HOW orderingHow = ORDERING_HOW.ASC;

        public enum ORDERING_BY {DEPARTURE_DATE, NAME, NONE}

        public enum ORDERING_HOW {ASC, DESC}

        public FlightFilter() {
        }

        public ORDERING_BY getOrderingBy() {
            return orderingBy;
        }

        public void setOrderingBy(ORDERING_BY orderingBy) {
            this.orderingBy = orderingBy;
        }

        public ORDERING_HOW getOrderingHow() {
            return orderingHow;
        }

        public void setOrderingHow(ORDERING_HOW orderingHow) {
            this.orderingHow = orderingHow;
        }

        public Date getDateOfDepartureFrom() {
            return dateOfDepartureFrom;
        }

        public void setDateOfDepartureFrom(Date dateOfDepartureFrom) {
            this.dateOfDepartureFrom = dateOfDepartureFrom;
        }

        public Date getDateOfDepartureTo() {
            return dateOfDepartureTo;
        }

        public void setDateOfDepartureTo(Date dateOfDepartureTo) {
            this.dateOfDepartureTo = dateOfDepartureTo;
        }

        public Integer getBase() {
            return base;
        }

        public void setBase(Integer base) {
            this.base = base;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }
    }
}
