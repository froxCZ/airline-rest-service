package cz.cvut.fel.aos.airline.model;

/**
 * Created by Jan on 28.10.2015.
 */
public enum State{
    NEW, PAID, CANCELED
}
