package cz.cvut.fel.aos.airline.jaxrs.resources;

import cz.cvut.fel.aos.airline.jaxrs.exceptions.*;
import cz.cvut.fel.aos.airline.jaxrs.service.FlightRepository;
import cz.cvut.fel.aos.airline.jaxrs.service.ReservationRepository;
import cz.cvut.fel.aos.airline.jaxrs.service.email.ws.SendReservationService;
import cz.cvut.fel.aos.airline.jaxrs.service.print.PrintService;
import cz.cvut.fel.aos.airline.jaxrs.service.print.PrintServiceService;
import cz.cvut.fel.aos.airline.model.Payment;
import cz.cvut.fel.aos.airline.model.State;
import cz.cvut.fel.aos.airline.model.reservation.Reservation;
import cz.cvut.fel.aos.airline.model.reservation.ReservationDTO;
import cz.cvut.fel.aos.airline.model.reservation.ReservationPassword;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path(ReservationResource.PATH)
public class ReservationResource {

    @Context
    UriInfo uriInfo;

    public static final String PATH = "service/reservation/";


    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<ReservationDTO> getAllReservations() throws Exception {
        return ReservationDTO.createDTOList(ReservationRepository.getAllReservations(), uriInfo);
    }


    @GET
    @Path("{reservationId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getReservationById(@PathParam("reservationId") long reservationId,  @HeaderParam("X-password") String password) throws Exception {
        try{
            ReservationDTO res=new ReservationDTO(ReservationRepository.getReservationById(reservationId), uriInfo);
            return Response.status(200).entity(res).build();
        } catch (ReservationNotFoundException reservationNotFoundExeption) {
            return Response.status(404).build();
        }
    }

    @POST
    @Path("{reservationId}/payment")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response payReservation(@PathParam("reservationId") long reservationId, Payment payment) throws Exception {
        try {
            ReservationRepository.payReservation(reservationId);
            return Response.status(200).build();
        } catch (ReservationNotFoundException reservationNotFoundExeption) {
            return Response.status(404).build();
        } catch (ReservationCanceledException e) {
            return Response.status(403).build();
        } catch (ReservationPaidException e) {
            return Response.status(403).build();
        }
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response newReservation(ReservationDTO reservationDTO) {
        try {
            ReservationPassword password=new ReservationPassword("passwd"+(int)Math.floor((Math.random() * 50) + 1), ReservationRepository.getReservationIdInc());
            Reservation res=new Reservation(reservationDTO);
            res.setPassword(password);
            ReservationRepository.addReservation(res);
            return Response.status(Response.Status.OK).entity(password).build();
        } catch (FlightFullException e) {
            return Response.status(403).build();
        }
        catch (FlightRepository.FlightNotFoundException e) {
            return Response.status(403).build();
        }

    }
    /*
    *Cancel reservation if state is new.
     */

    @PUT
    @Path("{reservationId}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateReservation(@PathParam("reservationId") Long reservationId) {
        try {
            ReservationRepository.cancelReservation(reservationId);
            return Response.status(200).build();
        } catch (ReservationNotFoundException reservationNotFoundExeption) {
            return Response.status(404).build();
        }
        catch (ReservationNotNewException reservationNotNewExeption) {
            return Response.status(400).build();
        }
    }

    @DELETE
    @Path("{reservationId}")
    @RolesAllowed({"admin"})
    public Response deleteReservation(@PathParam("reservationId") Long reservationId) {
        try{ReservationRepository.deleteReservation(reservationId);
        return Response.status(200).build();}
        catch (ReservationNotFoundException reservationNotFoundExeption){
            return Response.status(404).build();
        }
        catch (ReservationPaidException reservationPaidException){
            return Response.status(400).build();
        }
    }

    @GET
    @Path("{reservationId}/print")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getETicket(@PathParam("reservationId") Long reservationId) throws Exception {

        PrintServiceService printService = new PrintServiceService();
        PrintService print = printService.getPrintServicePort();
        cz.cvut.fel.aos.airline.jaxrs.service.print.Reservation printReservation = new cz.cvut.fel.aos.airline.jaxrs.service.print.Reservation();
        try {
            Reservation reservation = ReservationRepository.getReservationById(reservationId);
            if (reservation.getState() != State.PAID) {
                return Response.status(400).build();
            }

            printReservation.setId(reservation.getId());
            printReservation.setFlightId(reservation.getFlight());
            String s = print.printReservation(printReservation);


            String fileName = "e-ticket-" + printReservation.getId() + ".txt";
            return Response.ok(s, MediaType.TEXT_PLAIN_TYPE).header("content-disposition", "attachment; filename = " + fileName).build();
        } catch (ReservationNotFoundException e) {
            return Response.status(404).build();
        }
    }


    @POST
    @Path("{reservationId}/send")
    @Produces(MediaType.TEXT_PLAIN)
    public Response sendETicket(@PathParam("reservationId") Long reservationId, String email) throws Exception {


        try {
            Reservation reservation = ReservationRepository.getReservationById(reservationId);
            if (reservation.getState() != State.PAID) {
                return Response.status(400).build();
            }
            //generate file
            PrintServiceService printService = new PrintServiceService();
            PrintService print = printService.getPrintServicePort();
            cz.cvut.fel.aos.airline.jaxrs.service.print.Reservation printReservation = new cz.cvut.fel.aos.airline.jaxrs.service.print.Reservation();
            printReservation.setId(reservation.getId());
            printReservation.setFlightId(reservation.getFlight());
            String generatedTicket = print.printReservation(printReservation);

            //Email Ticket
            SendReservationService sendReservationService =new SendReservationService();
            sendReservationService.sendReservationEmail(email, generatedTicket);

            return Response.ok().build();
        } catch (ReservationNotFoundException e) {
            return Response.status(404).build();
        }
    }
}
