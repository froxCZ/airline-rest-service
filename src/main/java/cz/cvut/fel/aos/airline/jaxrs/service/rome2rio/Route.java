package cz.cvut.fel.aos.airline.jaxrs.service.rome2rio;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Jan on 14.12.2015.
 */
@XmlRootElement
public class Route {
    String name;
    float distance;
    float duration;
    Stop[] stops;
    Segment[] segments;

    public Route() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public Stop[] getStops() {
        return stops;
    }

    public void setStops(Stop[] stops) {
        this.stops = stops;
    }

    public Segment[] getSegments() {
        return segments;
    }

    public void setSegments(Segment[] segments) {
        this.segments = segments;
    }
}
