package cz.cvut.fel.aos.airline.jaxrs.resources;

import cz.cvut.fel.aos.airline.jaxrs.exceptions.DestionationNotFoundExeption;
import cz.cvut.fel.aos.airline.jaxrs.service.DestinationRepository;
import cz.cvut.fel.aos.airline.jaxrs.service.FlightRepository;
import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.GeocodeResponse;
import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.GeocodeService;
import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.GeocodedAddress;
import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.Location;
import cz.cvut.fel.aos.airline.model.destination.Destination;
import cz.cvut.fel.aos.airline.model.destination.DestinationDTO;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * Created by frox on 22.10.15.
 */
@Path(DestinationResource.PATH)
public class DestinationResource {
    public static final String PATH = "service/destination/";

    @Context
    UriInfo uriInfo;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<DestinationDTO> getAllDestinations(@HeaderParam("X-Order") String ordering) throws Exception {
        DestinationRepository.DestinationFilter destinationFilter=new DestinationRepository.DestinationFilter();
        if (ordering != null) {
            if (ordering.equals("asc")) {
                destinationFilter.setOrderingHow(DestinationRepository.DestinationFilter.ORDERING_HOW.ASC);
            } else if (ordering.equals("desc")) {
                destinationFilter.setOrderingHow(DestinationRepository.DestinationFilter.ORDERING_HOW.DESC);
            }
        }
        return DestinationDTO.createDTOList(DestinationRepository.getAllDestinations(destinationFilter), uriInfo);
    }

    @GET
    @Path("{destinationId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public DestinationDTO getDestinationById(@PathParam("destinationId") int destinationId) throws Exception {
        return new DestinationDTO(DestinationRepository.getDestinationById(destinationId), uriInfo);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response saveMessage(Destination destination) {
        Location coordinates = GeocodeService.getCoordinates(destination.getName());
        if (coordinates == null) {
            destination.setLat(0);
            destination.setLon(0);
        } else {
            destination.setLat(coordinates.getLat());
            destination.setLon(coordinates.getLng());
        }
        DestinationRepository.addDestination(destination);
        return Response.status(200).build();
    }

    @PUT
    @Path("{destinationId}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateDestination(@PathParam("destinationId") int destinationId, Destination destination) {
        try {
            DestinationRepository.updateDestination(destinationId, destination);
            return Response.status(200).build();
        } catch (DestionationNotFoundExeption destionationNotFoundExeption) {
            return Response.status(404).build();
        }
    }

    @DELETE
    @Path("{destinationId}")
    public Response deleteDestination(@PathParam("destinationId") int destinationId) {
        DestinationRepository.deleteDestination(destinationId);
        FlightRepository.deleteFlightsWithDestination(destinationId);
        return Response.status(200).build();
    }


}
