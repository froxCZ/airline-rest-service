/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.aos.airline.jaxrs.exceptions;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.*;

/**
 *
 * @author lubos
 */
/** Thrown to return a 403 Access denied response with a list of error messages in the body. */
public class AccessDeniedRequestException extends WebApplicationException
{
    private static final long serialVersionUID = 1L;
    private String error;
 
    public AccessDeniedRequestException(String error)
    {
        super(Response.status(Status.UNAUTHORIZED).type(MediaType.TEXT_PLAIN)
                .entity(error).build());
        this.error = error;
    }
 
    public String getError()
    {
        return error;
    }
}
