package cz.cvut.fel.aos.airline.helper;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by frox on 1.11.15.
 */
public class MyUtil {
    public static Date createDate(int year, int month, int day, int hour, int minute, int second) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.setTimeZone(TimeZone.getTimeZone("CET"));
        cal.set(year, month, day, hour, minute, second);
        return cal.getTime();
    }
}
