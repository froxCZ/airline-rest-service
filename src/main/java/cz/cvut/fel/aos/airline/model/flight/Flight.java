package cz.cvut.fel.aos.airline.model.flight;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by frox on 19.10.15.
 */

public class Flight {
    protected String name;
    protected long id;
    protected Date dateOfDeparture;
    protected Float price,distance;

    protected int from,to,seats;
    protected int seatsTaken;



    public Flight() {
    }

    /**
     *
     * @param name
     * @param dateOfDeparture
     * @param price
     * @param distance
     * @param from
     * @param to
     * @param seats
     */
    public Flight(String name, Date dateOfDeparture, Float price, Float distance, int from, int to, int seats) {
        this.name = name;
        this.dateOfDeparture = dateOfDeparture;
        this.price = price;
        this.distance = distance;
        this.from = from;
        this.to = to;
        this.seats = seats;
    }

    public Flight(FlightDTO flightDTO) {
        this.id = flightDTO.getId();
        updateFromDTO(flightDTO);
    }

    public void updateFromDTO(FlightDTO flightDTO) {
        from = flightDTO.getFrom();
        to = flightDTO.getTo();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("Europe/Prague"));
        try {
            dateOfDeparture = df.parse(flightDTO.getDateOfDeparture());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        name = flightDTO.getName();
        seats = flightDTO.getSeats();
        price = flightDTO.getPrice();
        distance = flightDTO.getDistance();
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfDeparture() {
        return dateOfDeparture;
    }

    public void setDateOfDeparture(Date dateOfDeparture) {
        this.dateOfDeparture = dateOfDeparture;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getSeatsTaken() {
        return seatsTaken;
    }

    public void setSeatsTaken(int seatsTaken) {
        this.seatsTaken = seatsTaken;
    }

    public boolean reserveSeats(int numberOfSeats){
        if(seatsTaken+numberOfSeats>seats)return false;
        else{
            seatsTaken+=numberOfSeats;
            return true;
        }
    }

    public void releaseSeats(int numberOfSeats){
        seatsTaken-=numberOfSeats;
    }


    public static class NameComparatorAsc implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            Flight f1 = (Flight) o1;
            Flight f2 = (Flight) o2;
            return f1.getName().compareTo(f2.getName());
        }
    }

    public static class NameComparatorDesc implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            Flight f1 = (Flight) o1;
            Flight f2 = (Flight) o2;
            return f2.getName().compareTo(f1.getName());
        }
    }

    public static class DepartureDateComparatorAsc implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            Flight f1 = (Flight) o1;
            Flight f2 = (Flight) o2;
            return f1.getDateOfDeparture().compareTo(f2.getDateOfDeparture());
        }
    }

    public static class DepartureDateComparatorDesc implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            Flight f1 = (Flight) o1;
            Flight f2 = (Flight) o2;
            return f2.getDateOfDeparture().compareTo(f1.getDateOfDeparture());
        }
    }


}
