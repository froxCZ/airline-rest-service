package cz.cvut.fel.aos.airline.model.destination;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Comparator;

/**
 * Created by frox on 19.10.15.
 */
@XmlRootElement
public class Destination {
    long id;
    String name;
    double lat;
    double lon;

    /**
     * Class used as XML must have empty constructor! Otherwise 500 error with no message is thrown!
     */
    public Destination() {
    }

    public Destination(String name, double lat, double lon) {
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public static class NameComparatorAsc implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            Destination d1 = (Destination) o1;
            Destination d2 = (Destination) o2;
            return d1.getName().compareTo(d2.getName());
        }
    }

    public static class NameComparatorDesc implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            Destination d1 = (Destination) o1;
            Destination d2 = (Destination) o2;
            return d2.getName().compareTo(d1.getName());
        }
    }
}
