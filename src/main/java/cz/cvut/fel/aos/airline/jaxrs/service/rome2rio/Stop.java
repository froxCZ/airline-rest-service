package cz.cvut.fel.aos.airline.jaxrs.service.rome2rio;

import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.Location;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Jan on 14.12.2015.
 */
@XmlRootElement
public class Stop {
    String kind;
    String name;
    Location pos;
    String code;
    String countryCode;
    String regionCode;
    String timeZome;

    public Stop() {
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getPos() {
        return pos;
    }

    public void setPos(Location pos) {
        this.pos = pos;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getTimeZome() {
        return timeZome;
    }

    public void setTimeZome(String timeZome) {
        this.timeZome = timeZome;
    }
}
