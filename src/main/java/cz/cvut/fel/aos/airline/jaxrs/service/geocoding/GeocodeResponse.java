package cz.cvut.fel.aos.airline.jaxrs.service.geocoding;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by frox on 12.12.15.
 */
@XmlRootElement
public class GeocodeResponse {
    private List<GeocodedAddress> results;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GeocodeResponse() {
    }

    public List<GeocodedAddress> getResults() {
        return results;
    }

    public void setResults(List<GeocodedAddress> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "GeocodeResponse{" +
                "results=" + results +
                '}';
    }
}
