package cz.cvut.fel.aos.airline.jms;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;


import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;

import java.io.Serializable;
import javax.jms.*;

@MessageDriven(name = "JmsReceiver", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "myQueue"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
})
public class JmsReceiver implements MessageListener {
    public static final String EMAIL_MSG_RECIPIENT_KEY = "recipient";
    public static final String EMAIL_MSG_CONTENT_KEY = "content";
    public static ArrayList<String> txtmessages=new ArrayList<>();

    private final static Logger LOGGER = Logger.getLogger(JmsReceiver.class.toString());

    public void onMessage(Message rcvMessage) {
        try {
            if (rcvMessage instanceof TextMessage) {
                TextMessage msg = (TextMessage) rcvMessage;
                LOGGER.info("Received Message from queue: " + msg.getText());
                txtmessages.add(msg.getText());
            } else if (rcvMessage instanceof ObjectMessage) {
                ObjectMessage msg = (ObjectMessage) rcvMessage;
                MessageWrapper wraper = (MessageWrapper) msg.getObject();
                LOGGER.info("Received Message from queue: " + wraper.toString());
            } else if (rcvMessage instanceof BytesMessage) {
                BytesMessage msg = (BytesMessage) rcvMessage;
                byte[] rcvBytes = new byte[(int) msg.getBodyLength()];
                msg.readBytes(rcvBytes);
                String recipient = msg.getStringProperty(EMAIL_MSG_RECIPIENT_KEY);
                String content = msg.getStringProperty(EMAIL_MSG_CONTENT_KEY);
                StringBuilder sb = new StringBuilder();
                sb.append("Received BytesMessage(email) from queue: recipient:" + recipient + " content:" + content + " attachedBytes:");
                for (byte b : rcvBytes) {
                    sb.append(String.format("%02X ", b)); //hex
                    sb.append(" ");
                }
                sb.append(" attachedBytesInString:" + new String(rcvBytes, StandardCharsets.UTF_8));
                LOGGER.info(sb.toString());
            } else {
                LOGGER.warning("Message of wrong type: " + rcvMessage.getClass().getName());
            }
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }
}



