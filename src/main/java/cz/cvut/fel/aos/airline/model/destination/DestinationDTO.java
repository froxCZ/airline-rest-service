package cz.cvut.fel.aos.airline.model.destination;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by frox on 19.10.15.
 */
@XmlRootElement
public class DestinationDTO {
    long id;
    String name;
    double lat;
    double lon;
    String url;

    /**
     * Class used as XML must have empty constructor! Otherwise 500 error with no message is thrown!
     */
    public DestinationDTO() {
    }


    public DestinationDTO(Destination destination, UriInfo uriInfo) {
        this.id = destination.getId();
        this.name = destination.getName();
        this.lat = destination.getLat();
        this.lon = destination.getLon();
        url = uriInfo.getBaseUriBuilder().path("service/destination").build().getPath() + "/" + getId();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public static List<DestinationDTO> createDTOList(List<Destination> destinationList, UriInfo uriInfo) {
        LinkedList<DestinationDTO> destinationDTOs = new LinkedList<>();
        for (Destination destination : destinationList) {
            destinationDTOs.add(new DestinationDTO(destination, uriInfo));
        }
        return destinationDTOs;
    }
}
