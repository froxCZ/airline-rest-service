package cz.cvut.fel.aos.airline.jaxrs.service.rome2rio;

/**
 * Created by Jan on 14.12.2015.
 */
public class Path {
    String path;

    public Path() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
