package cz.cvut.fel.aos.airline.jaxrs.service.rome2rio;

import cz.cvut.fel.aos.airline.jaxrs.service.geocoding.Location;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Jan on 15.12.2015.
 */
@XmlRootElement
public class Place {
    String kind;
    String name;
    String longName;
    Location pos;
    String countryCode;
    String regionCode;
    String timeZone;

    public Place() {
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public Location getPos() {
        return pos;
    }

    public void setPos(Location pos) {
        this.pos = pos;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
