package cz.cvut.fel.aos.airline.model.reservation;

import cz.cvut.fel.aos.airline.model.State;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;


@XmlRootElement
public class Reservation {
    Long  id;
    Long  flight; //Flight id
    Date created;
    ReservationPassword password;
    int seats;
    State state;


    /**
     * Class used as XML must have empty constructor! Otherwise 500 error with no message is thrown!
     */
    public Reservation() {
    }

    public Reservation(Long id, Long flight, ReservationPassword password, int seats) {
        this.id = id;
        this.flight = flight;
        this.created = new Date();
        this.password = password;
        this.seats = seats;
        this.state = State.NEW;
    }

    public Reservation(ReservationDTO reservationDTO) {
        this.id = reservationDTO.getId();
        this.flight = reservationDTO.getFlight();
        this.created = new Date();
        this.seats = reservationDTO.getSeats();
        this.state = State.NEW;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFlight() {
        return flight;
    }

    public void setFlight(Long flight) {
        this.flight = flight;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public ReservationPassword getPassword() {
        return password;
    }

    public void setPassword(ReservationPassword password) {
        this.password = password;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}

