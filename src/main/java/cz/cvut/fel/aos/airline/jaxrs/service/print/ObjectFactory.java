
package cz.cvut.fel.aos.airline.jaxrs.service.print;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cz.fel.cvut.aos.printservice.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Reservation_QNAME = new QName("http://service.printservice.aos.cvut.fel.cz/", "reservation");
    private final static QName _PrintReservationResponse_QNAME = new QName("http://service.printservice.aos.cvut.fel.cz/", "printReservationResponse");
    private final static QName _PrintReservation_QNAME = new QName("http://service.printservice.aos.cvut.fel.cz/", "printReservation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cz.fel.cvut.aos.printservice.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PrintReservation }
     * 
     */
    public PrintReservation createPrintReservation() {
        return new PrintReservation();
    }

    /**
     * Create an instance of {@link Reservation }
     * 
     */
    public Reservation createReservation() {
        return new Reservation();
    }

    /**
     * Create an instance of {@link PrintReservationResponse }
     * 
     */
    public PrintReservationResponse createPrintReservationResponse() {
        return new PrintReservationResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Reservation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.printservice.aos.cvut.fel.cz/", name = "reservation")
    public JAXBElement<Reservation> createReservation(Reservation value) {
        return new JAXBElement<Reservation>(_Reservation_QNAME, Reservation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrintReservationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.printservice.aos.cvut.fel.cz/", name = "printReservationResponse")
    public JAXBElement<PrintReservationResponse> createPrintReservationResponse(PrintReservationResponse value) {
        return new JAXBElement<PrintReservationResponse>(_PrintReservationResponse_QNAME, PrintReservationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrintReservation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.printservice.aos.cvut.fel.cz/", name = "printReservation")
    public JAXBElement<PrintReservation> createPrintReservation(PrintReservation value) {
        return new JAXBElement<PrintReservation>(_PrintReservation_QNAME, PrintReservation.class, null, value);
    }

}
