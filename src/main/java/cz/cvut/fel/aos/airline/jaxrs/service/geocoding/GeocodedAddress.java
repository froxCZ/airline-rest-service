package cz.cvut.fel.aos.airline.jaxrs.service.geocoding;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by frox on 12.12.15.
 */
@XmlRootElement
public class GeocodedAddress {
    private Geometry geometry;

    public GeocodedAddress() {
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}
