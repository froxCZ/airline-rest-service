package cz.cvut.fel.aos.airline.jaxrs.service.geocoding;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jndi.toolkit.url.Uri;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by frox on 12.12.15.
 */
public class GeocodeService {

    /**
     * returns Location object with coordinates or null
     * @param place
     * @return
     */
    public static Location getCoordinates(String place) {
        Client client = Client.create();
        URL url = null;
        try {
            WebResource webResource = client.resource("https://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(place, "UTF-8") + "&key=AIzaSyDZdEz1HptX2Ldp5f4HhknwbTlIJeN5Fb4");
            ClientResponse response = webResource.accept("application/json")
                    .get(ClientResponse.class);
            if (response.getStatus() == 200) {
                GeocodeResponse geocodeResponse = response.getEntity(GeocodeResponse.class);
                if (geocodeResponse.getStatus().equalsIgnoreCase("ok") &&
                        geocodeResponse.getResults().size() > 0) {
                    return geocodeResponse.getResults().get(0).getGeometry().getLocation();

                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
